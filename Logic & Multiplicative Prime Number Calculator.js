//--------------------------------------------------------------------------------------
//prime number rules
// 1) number is an integer
// 2) cannot be a product of any other integers besides 1 and itself or divisible without a remainder
// 3) the number 2 is an exception
// 4) all other prime numbers not including 2 are odd

//This calculator only uses multiplication and logic to find prime numbers. 
//It takes 62.456 seconds to find prime numbers between 0 and 10,000.
//This calculator just outputs to the console in a prime array length and an array list.
//Tried to limit the use of javascript methods in order to provide a more difficult challenge.
const start = new Date()
let primeFinder = (primeSpan) => {
let limit  = primeSpan
for (i = 0; i <= limit; i++) {
    if (2 * i == limit) {
        var factorSpan = i
    }
}
let oddAr = [2]
for (let g = 3; g <= limit; g = g + 2) {
    oddAr.push(g)
}
let factors = []
for (let i = 1; i <= limit; i = i + 2) {
    for (let h = 0; h < factorSpan; h = h + 1) {
        for (let k = h; k < factorSpan; k = k + 1) {
            if (k * h == i && k > 1 && h > 1) {
                factors.push(i)
            }
        }
    } 
}
for (x = 0; x <= oddAr.length; x++) {
    for (z = 0; z <= factors.length; z++){
        if (oddAr[x] - factors[z] == 0) {
            oddAr.splice(x, 1)
        }
    }
} 
console.log(oddAr.length)
console.log(oddAr)
}
primeFinder(100000)
const end = new Date()
console.log(`${(end.getTime() - start.getTime()) * 0.001} seconds`)
//------------------------------------------------------------------------------------------------------
